#!/usr/bin/env -S swift
/**
*  Pipeline Engine
*
*  This file and its contents are distributed under the terms of the
*  Common Development and Distribution License, Version 1.0 (the "License").
*  You may not use this file except in compliance with the License.
*
*  You may obtain a copy of the License in the included file,
*  LICENSES/CDDL-1.0.txt or online at:
*
*     http://opensource.org/licenses/cddl1
*
*  See the License for the specific language governing permissions and
*  limitations.
*
*  Copyright: 2023 Drew Adams <druonysus>
*  License: CDDL-1.0
*  Authors: Drew Adams
*/

import Foundation
import ArgumentParser

func printVersion(format: String) {
    switch format {
        case "short":
            print(PipelineEngine.versionNumber)

        case "long":
            print("Pipeline Engine v\(PipelineEngine.versionNumber)\n")
            print("Copyright   : \(Calendar.current.component(.year, from: Date())) Drew Adams <druonysus@opensuse.org>")
            print("License     : Common Development and Distribution License 1.0 (CDDL-1.0)")
            print("Source Code : https://gitlab.com/druonysus/pipeline-engine")
        default:
            // TODO: actually throw an error
            print("Unknown version format requested!")
    }
}

struct PipelineEngine: ParsableCommand {
    static let programName = "Pipeline Engine"
    static let versionNumber = "0.0.1"

    static let configuration = CommandConfiguration(abstract: "Runs tasks in a pipeline based on environment variables and numbered executable files.")

    @Option(name: .shortAndLong, help: "The directory to search for environment and pipeline files.")
    var directory: String = FileManager.default.currentDirectoryPath

    @Flag(name: .shortAndLong, help: "Run pipeline tasks sequentially instead of in parallel.")
    var noParallel: Bool = false

    @Option(name: .shortAndLong, help: "The directory to use for pipeline log files.")
    var logDirectory: String?

    @Option(name: .shortAndLong, help: "The directory to use for pipeline executables.")
    var pipelineDirectory: String?

    @Option(name: .shortAndLong, help: "The environment to use for running the pipeline tasks.")
    var environment: String = "dev"

    @Flag(name: .short, help: "Prints the version and exits.")
    var versionFlag: Bool = false

    @Flag(name: .long, help: "Prints the version, copyright notice and code link.")
    var version: Bool = false

    func run() throws {
        if versionFlag {
            printVersion(format: "short")
            return
        }
        else if version {
            printVersion(format: "long")
            return
        }
        
        // Set up log directory
        let logDirectoryURL = URL(fileURLWithPath: logDirectory ?? directory.appendingPathComponent(".pipeline-logs"))
        try FileManager.default.createDirectory(at: logDirectoryURL, withIntermediateDirectories: true)

        // Find environment file and parse it
        let environmentURL = URL(fileURLWithPath: directory + "/.\(environment).env")
        let environmentData = try String(contentsOf: environmentURL, encoding: .utf8)
        var environment = [String: String]()
        environmentData.enumerateLines { line, _ in
            if line.starts(with: "#") || line.starts(with: "//") || line.isEmpty {
                return
            }
            let parts = line.split(separator: "=", maxSplits: 1, omittingEmptySubsequences: false)
            if parts.count == 2 {
                environment[String(parts[0])] = String(parts[1])
            }
        }

        // Set environment variables
        environment.forEach { key, value in
            setenv(key, value, 1)
        }

        // Find and sort pipeline files
        let pipelineDirectoryURL = URL(fileURLWithPath: pipelineDirectory ?? directory.appendingPathComponent(".pipeline"))
        let pipelineFiles = try FileManager.default.contentsOfDirectory(at: pipelineDirectoryURL, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            .filter { $0.lastPathComponent.range(of: #"^\d{1,3}\.\d{1,3}-.*"#, options: .regularExpression) != nil }
            .sorted { $0.lastPathComponent < $1.lastPathComponent }
        
        // FOR DEVELOPMENT ONLY: check the contents of pipelineFiles
        print(pipelineFiles)

        // Run pipeline tasks in order
        for pipelineFile in pipelineFiles {
            let stage = String(pipelineFile.lastPathComponent.split(separator: "-").first!)
            print("[** INFO] Running \(pipelineFile.lastPathComponent) (Stage \(stage))")
            let task = Process()
            task.executableURL = pipelineFile
            task.standardOutput = try FileHandle(forWritingTo: logDirectoryURL.appendingPathComponent("\(stage).log"))
            task.standardError = task.standardOutput
            try task.run()
            task.waitUntilExit()

            if task.terminationStatus != 0 {
                print("[** ERROR] \(pipelineFile.lastPathComponent) failed with exit code \(task.terminationStatus).")
                break
            }
        }
    }
}

PipelineEngine.main()