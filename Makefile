#  Makefile for Pipeline Engine
#
#  This file and its contents are distributed under the terms of the
#  Common Development and Distribution License, Version 1.0 (the "License").
#  You may not use this file except in compliance with the License.
#
#  You may obtain a copy of the License in the included file,
#  LICENSES/CDDL-1.0.txt or online at:
#
#     http://opensource.org/licenses/cddl1
#
#  See the License for the specific language governing permissions and
#  limitations.
#
#  Copyright: 2023 Drew Adams <druonysus@opensuse.org>
#  License: CDDL-1.0
#  Authors: Drew Adams

debug-build:
	swift build

release-build:
	swift build -c release

install: release-build
	# TODO: Support more than just x86 Linux
	install -m 700 ./.build/x86_64-unknown-linux-gnu/release/pipeline-engine ~/.local/bin/