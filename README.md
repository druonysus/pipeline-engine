# Pipeline Engine

This is a command-line tool for running tasks in a pipeline based on environment variables and numbered executable files.

## Installation

To install the Pipeline Engine, simply clone this repository and build the executable using the Swift Package Manager:

```
git clone https://gitlab.com/druonysus/pipeline-engine.git
cd pipeline-engine
make install
```

## Pipeline Files

Pipeline files are numbered executable files located in a directory named `.pipeline` inside the directory specified with the `--directory` option. The naming convention for pipeline files is `XX.XX-task-name`, where `XX.XX` indicates the stage number the task belongs to, and where `task-name` is a short descriptive name for the task.

For example, a pipeline file in the first stage might be named `00.00-setup`, and a pipeline file in the next stage might be named `00.01-lint`.

Pipeline files are executed in order according to their stage number. If multiple tasks have the same stage number prefix, those tasks can be run in parallel.

## Environment Files

Environment files are located in the same directory as the pipeline files and are named `.ENVIRONMENT_NAME.env`, where `ENVIRONMENT_NAME` is the name of the environment (e.g., dev, test, prod).

Environment variables are loaded from the environment file for the specified environment and are set before each pipeline task is run.

## License Summary

Pipeline Engine is licensed under the Common Development and Distribution License 1.0. See [LICENSES/CDDL-1.0.txt](LICENSES/CDDL-1.0.txt) for the full license text.

Examples and configuration files are licensed under The Unlicense. See [LICENSES/Unlicense.txt](LICENSES/Unlicense.txt) for the full license text.

Documentation is licensed under the Common Documentation License 1.0. See [LICENSES/CDL-1.0.txt](LICENSES/CDL-1.0.txt) for the full license text.

----

Copyright © 2023 Drew Adams <<druonysus@opensuse.org>>.

This material has been released under and is subject to the terms of the
Common Documentation License, v.1.0, the terms of which are hereby
incorporated by reference. Please obtain a copy of the License at
https://spdx.org/licenses/CDL-1.0.html and read it before using this
material. Your use of this material signifies your agreement to the terms of
the License.